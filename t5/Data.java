package t5;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Data {
	
	LocalDateTime startTime;
	LocalDateTime endTime;
	String activityLabel;
	
	public Data(LocalDateTime start, LocalDateTime end, String activity){
		
		this.startTime = start;
		this.endTime = end;
		this.activityLabel = activity;
	}
	
	public 	void setStartTime(LocalDateTime date){
		this.startTime = date;
	}
	
	public LocalDateTime getStartTime(){
		return startTime;
	}
	
	
	public void setEndTime(LocalDateTime date){
		this.endTime = date;
	}
	
	public LocalDateTime getEndTime(){
		return endTime;
	}
	
	public void setActivityLabel(String activity){
		this.activityLabel = activity;
	}
	
	public String getActivityLabel(){
		return activityLabel;
	}
	
	public int getHours(){
		int Hours = 0;
		
		LocalDateTime fromDateTime = this.startTime;
		LocalDateTime toDateTime = this.endTime;
		
		if(toDateTime.getDayOfMonth()-fromDateTime.getDayOfMonth()!=0) {
			return 100;
		}
		else Hours=toDateTime.getHour()-fromDateTime.getHour();
		
		return Hours;
	}

	public int getMinutes() {

int ResultInMinutes = 0;
		
		LocalDateTime fromDateTime = this.startTime;
		LocalDateTime toDateTime = this.endTime;
		
		if(toDateTime.getHour()-fromDateTime.getHour()!=0) {
			return 100;
		}
		else ResultInMinutes=toDateTime.getMinute()-fromDateTime.getMinute();
		return ResultInMinutes;
	}
	
	
}